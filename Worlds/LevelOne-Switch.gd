extends Area2D

## NOTICE ##
# MODIFIED VARIANT OF Lever.gd
# Is a desperate last minute implementation for demo
# TODO make lever class send done signal

var enabled = true
var rewinding = false

func _process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "Player" && Input.is_action_just_pressed("ui_interact"):
			enabled = !enabled 
			get_parent().get_node("Moving Platform").speed = - get_parent().get_node("Moving Platform").speed
			get_parent().get_node("Spawner").enabled = true
			if enabled:
				$Sprite.animation = "off"
			else:
				$Sprite.animation = "on"


